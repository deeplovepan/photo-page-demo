//
//  PageDataViewController.h
//  HanaMini
//
//  Created by Pan Peter on 12/11/17.
//  Copyright (c) 2012年 Pan Peter. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PageDataViewController : UIViewController

@property (nonatomic) int pageIndex;
@property (nonatomic, strong) NSString *startFileName;
@property (strong, nonatomic) id dataObject;

@end
