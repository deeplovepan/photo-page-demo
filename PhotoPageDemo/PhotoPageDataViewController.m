//
//  PennyPhotoPageDataViewController.m
//  PennyPhotoPage
//
//  Created by  on 11/11/7.
//  Copyright (c) 2011年 __MyCompanyName__. All rights reserved.
//

#import "PhotoPageDataViewController.h"

@implementation PhotoPageDataViewController


@synthesize photoImageView = _photoImageView;

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}


#pragma mark - View lifecycle


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.


   
    
    self.photoImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, [UIScreen mainScreen].bounds.size.height - 64)];
    self.photoImageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.view addSubview:self.photoImageView];
    
    NSString *name = [NSString stringWithFormat:@"%@.png", self.dataObject];
    
    UIImage *image = [UIImage imageNamed:name];
    
    
    self.photoImageView.image = image;

   


}

- (void)viewDidUnload
{
    [self setPhotoImageView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}


@end
