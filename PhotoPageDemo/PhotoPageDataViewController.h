//
//  PennyPhotoPageDataViewController.h
//  PennyPhotoPage
//
//  Created by  on 11/11/7.
//  Copyright (c) 2011年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PageDataViewController.h"

@interface PhotoPageDataViewController : PageDataViewController
{
    UIImageView *backgroundView;
}

@property (strong, nonatomic) UIImageView *photoImageView;



@end
