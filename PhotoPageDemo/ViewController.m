//
//  ViewController.m
//  PhotoPageDemo
//
//  Created by Peter Pan on 9/4/14.
//  Copyright (c) 2014 Peter Pan. All rights reserved.
//

#import "ViewController.h"
#import "PageRootViewController.h"
#import "PageModelController.h"


@interface ViewController ()

@end

@implementation ViewController


- (IBAction)showPhotos:(id)sender {
    
    PageRootViewController *controller = [[PageRootViewController alloc] init];
    controller.transitionStyle = UIPageViewControllerTransitionStyleScroll;
    controller.pageSpace = 10;
    controller.modelController.dataArray = @[@"pic0", @"pic1"];
    controller.startIndex = 0;
    controller.modelController.startFileName = @"";
    controller.modelController.dataViewClassString = @"PhotoPageDataViewController";
    controller.view.backgroundColor = [UIColor whiteColor];
    [self.navigationController pushViewController:controller animated:YES];

}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
