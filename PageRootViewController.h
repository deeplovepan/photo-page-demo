//
//  PennyPhotoPageRootViewController.h
//  PennyPhotoPage
//
//  Created by  on 11/11/7.
//  Copyright (c) 2011年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PageModelController;

@interface PageRootViewController : UIViewController <UIPageViewControllerDelegate>

@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (strong, nonatomic) PageModelController *modelController;
@property (assign, nonatomic) int startIndex;
@property (assign, nonatomic) UIPageViewControllerTransitionStyle transitionStyle;
@property (assign, nonatomic) float pageSpace;

@end
