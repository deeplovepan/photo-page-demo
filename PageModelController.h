//
//  PennyPhotoPageModelController.h
//  PennyPhotoPage
//
//  Created by  on 11/11/7.
//  Copyright (c) 2011年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PageDataViewController;

@interface PageModelController : NSObject <UIPageViewControllerDataSource>

@property (nonatomic, strong) NSString *startFileName;
@property (nonatomic, strong) NSString *dataViewClassString;
@property (strong, nonatomic) NSMutableArray *dataArray;

- (PageDataViewController*)viewControllerAtIndex:(NSUInteger)index;
- (NSUInteger)indexOfViewController:(PageDataViewController *)viewController;
@end
