//
//  PennyPhotoPageModelController.m
//  PennyPhotoPage
//
//  Created by  on 11/11/7.
//  Copyright (c) 2011年 __MyCompanyName__. All rights reserved.
//

#import "PageModelController.h"
#import "PageDataViewController.h"

/*
 A controller object that manages a simple model -- a collection of month names.
 
 The controller serves as the data source for the page view controller; it therefore implements pageViewController:viewControllerBeforeViewController: and pageViewController:viewControllerAfterViewController:.
 It also implements a custom method, viewControllerAtIndex: which is useful in the implementation of the data source methods, and in the initial configuration of the application.
 
 There is no need to actually create view controllers for each page in advance -- indeed doing so incurs unnecessary overhead. Given the data model, these methods create, configure, and return a new view controller on demand.
 */

@interface PageModelController()
@property (readonly, strong, nonatomic) NSArray *pageData;
@end

@implementation PageModelController


- (id)init
{
    self = [super init];
    if (self) {
        // Create the data model.
    }
    return self;
}

- (PageDataViewController*)viewControllerAtIndex:(NSUInteger)index
{
   
    PageDataViewController *dataViewController = [[NSClassFromString(self.dataViewClassString) alloc] init];
    dataViewController.pageIndex = index;
    dataViewController.startFileName = self.startFileName;
    dataViewController.dataObject = [self.dataArray objectAtIndex:index];

    return dataViewController;
}

- (NSUInteger)indexOfViewController:(PageDataViewController *)viewController
{   
    /*
     Return the index of the given data view controller.
     For simplicity, this implementation uses a static array of model objects and the view controller stores the model object; you can therefore use the model object to identify the index.
     */
    //return [self.pageData indexOfObject:viewController.dataObject];
    return viewController.pageIndex;
}



#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = [self indexOfViewController:(PageDataViewController*)viewController];
    
    
    
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
    PageDataViewController *controller = [self viewControllerAtIndex:index];
    return controller;
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = [self indexOfViewController:(PageDataViewController*)viewController];

    
    if (index == NSNotFound) {
        return nil;
    }
    
    index++;
    if (index == self.dataArray.count) {
        return nil;
    }
    PageDataViewController *controller = [self viewControllerAtIndex:index];
    return controller;
}

@end
